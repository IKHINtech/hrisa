from flask import render_template, request, redirect, url_for, flash
from flask_login import login_required
from . import employee
from .. import db
from ..models import ResCountry,User, ResPartner, Role, HrDepartment, HrEmployee, HrJobs
from app import engine

@employee.route('/')
def index():
    emp_data = HrEmployee.query.all()
    return render_template('/hr_employee/index.html', data = emp_data)

@employee.route('/department')
def department():
    return render_template('hr_employee/departments/department.html')

@employee.route('/jobs')
def jobs():
    jobs = HrJobs.query.all()
    return render_template('hr_employee/jobs/jobs.html', data=jobs)

@employee.route('/jobs_add', methods=['GET', 'POST'])
def add_jobs():
    name = request.form.get('name')
    dept = request.form.get('dept')
    desc = request.form.get('desc')
    dept_data = HrDepartment.query.filter_by(name=dept).first()
    data = HrJobs(id=1, name=name, jobs_department=dept_data, description= desc)
    db.session.add(data)
    db.session.commit()
    flash('Data Jobs berhasil ditambah')
    return redirect(url_for('employee.jobs'))

@employee.route('/country')
def country():
    return render_template('hr_employee/country/country.html')
