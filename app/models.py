from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import db, login_manager
from sqlalchemy import Sequence
from datetime import datetime

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role', lazy='dynamic')#child of user

    def __repr__(self):
        return '<Role %r>' % self.name

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    employee = db.relationship('HrEmployee', backref='user', lazy='dynamic')#child of employee

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User %r>' % self.username

class HrDepartment(db.Model):
    __tablename__ = 'hr_department'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    active =db.Column(db.Boolean, default=True, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey(id))#ok
    manager_id = db.Column(db.Integer, db.ForeignKey('hr_employee.id'))#ok
    note = db.Column(db.Text())
    create_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    write_date = db.Column(db.DateTime(), index=True)
    dept_employee = db.relationship('HrEmployee', backref='departments', lazy='dynamic', 
                                        primaryjoin="and_(HrDepartment.id==HrEmployee.department_id) ")#child of employee
    partent_dept = db.relationship('HrDepartment', backref='subordinates', remote_side=id)#child of self / paretns
    jobs_deps = db.relationship('HrJobs', backref='jobs_department', lazy='dynamic')

    # def __repr__(self):
    #     return '<Hr_department %r>' % self.name

class ResCountry(db.Model):
    __tablename__ = 'res_country'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    codesname =db.Column(db.String(64))
    phone_code = db.Column(db.Integer)
    vat_label = db.Column(db.String(16))
    create_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    write_date = db.Column(db.DateTime(), index=True)
    employee_country = db.relationship('HrEmployee', backref='employe_country_of_birth', lazy='dynamic')#child of employee
    country_dartner = db.relationship('ResPartner', backref='partner_country', lazy='dynamic')#child of partner
    # def __repr__(self):
    #     return '<ResCountry %r>' % self.name

class ResPartner(db.Model):
    __tablename__ = 'res_partner'
    id = db.Column(db.Integer, primary_key= True)
    name = db.Column(db.String(64))
    create_date = db.Column(db.DateTime(), index=True, default= datetime.utcnow)
    street = db.Column(db.String(64))
    street2 = db.Column(db.String(64))
    zip_code = db.Column(db.Integer)
    city = db.Column(db.String(64))
    country_id = db.Column(db.Integer, db.ForeignKey('res_country.id'))#ok
    email = db.Column(db.String(64), unique=True, index=True)
    phone = db.Column(db.String(32))
    mobile = db.Column(db.String(32))
    write_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    employee_address = db.relationship('HrEmployee', backref='addresses', lazy='dynamic',
                                        primaryjoin="and_(ResPartner.id==HrEmployee.address_home_id) ")#child of employee
    
    # def __repr__(self):
    #     return '<ResPartner %r>' & self.name

class HrEmployee(db.Model):
    __tablename__ = 'hr_employee'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))#ok
    active = db.Column(db.Boolean, default=True, nullable= False)
    address_home_id = db.Column(db.Integer, db.ForeignKey('res_partner.id'))#ok
    gender = db.Column(db.String(16))
    martinal = db.Column(db.String(16))
    spouse_complete_name= db.Column(db.String(64))
    spouse_birthdate = db.Column(db.Date)
    children = db.Column(db.Integer)
    place_of_birth = db.Column(db.String(64))
    country_of_birth = db.Column(db.Integer, db.ForeignKey('res_country.id'))#ok
    birthday = db.Column(db.Date)
    identification_id = db.Column(db.String(64))
    passport_id = db.Column(db.String(64))
    bank_account_id = db.Column(db.String(64))
    permit_id = db.Column(db.String(64))
    additional_note = db.Column(db.Text)#status karyawan
    certificate = db.Column(db.String(32))
    study_filed = db.Column(db.String(64))
    study_school = db.Column(db.String(64))
    emergency_contact = db.Column(db.String(32))
    emergency_phone = db.Column(db.String(32))
    km_home_work = db.Column(db.Integer)
    notes = db.Column(db.Text)
    pin = db.Column(db.String(64))
    department_id = db.Column(db.Integer, db.ForeignKey('hr_department.id'))#ok
    jobs_id = db.Column(db.Integer, db.ForeignKey('hr_jobs.id'))#ok
    jobs_title = db.Column(db.String(64))
    address_id = db.Column(db.Integer, db.ForeignKey('res_partner.id'))
    work_phone = db.Column(db.String(32))
    mobile_phone = db.Column(db.String(32))
    work_email = db.Column(db.String(64))
    work_location = db.Column(db.String(64))
    parent_id = db.Column(db.Integer, db.ForeignKey(id))#ok
    coach_id = db.Column(db.Integer, db.ForeignKey(id))#ok
    create_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    write_date = db.Column(db.DateTime(), index=True)
    dept_manager = db.relationship('HrDepartment', backref='managers', lazy='dynamic',
                                        primaryjoin="and_(HrEmployee.id==HrDepartment.manager_id)")#child
    parents_em = db.relationship('HrEmployee', 
                                    foreign_keys='HrEmployee.parent_id',
                                    backref=db.backref('parents', remote_side=id))#foreign_keys=[parent_id])#child
    coach_em = db.relationship('HrEmployee', 
                                    foreign_keys='HrEmployee.coach_id', 
                                    backref=db.backref('coachs', remote_side=id))#child
    
    # def __repr__(self):
    #     return '<ResEmployee %r>' & self.name

class HrJobs(db.Model):
    __tablename__ = 'hr_jobs'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.Text)
    department_id = db.Column(db.Integer, db.ForeignKey('hr_department.id'))#ok
    create_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    write_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    employee = db.relationship('HrEmployee', backref='jobs', lazy='dynamic')

    # def __repr__(self):
    #     return '<HrJobs %r>' & self.name

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

