from flask import render_template
from flask_login import login_required
from . import main
from .. import db
from ..models import ResCountry,User, ResPartner, Role, HrDepartment, HrEmployee, HrJobs
from app import engine





@main.route('/')
@login_required
def index():
    ind = ResCountry.query.all()
    return render_template('index.html', data = ind)

@main.route('/profile')
@login_required
def profile():
    return render_template('/profile/profile.html')