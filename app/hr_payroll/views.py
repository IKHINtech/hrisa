from flask import render_template
from flask_login import login_required
from . import payroll
from .. import db
from ..models import ResCountry, ResPartner, HrDepartment, HrEmployee, HrJobs, Role, User

@payroll.route('/')
def main_payroll():
    return render_template('/hr_payroll/index.html')